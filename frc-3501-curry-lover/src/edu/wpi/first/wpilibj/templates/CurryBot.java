/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
package edu.wpi.first.wpilibj.templates;

import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStationLCD;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class CurryBot extends IterativeRobot {
    // quickchange parameters

    final String SERVER_IP = "10.35.1.34";
    final int SERVER_PORT = 1180;

    final double GOAL_LENGTH = 36;
    // ultrasonic sensor
    final double MV_PER_INCH = 9.8;
    // ultrasonic sensor ANGLE
    final double ANGLE = 0;

    // drive base
    final double SECS_TO_FULL_SPEED = 1.5;
    final int FL_PORT = 2, FR_PORT = 4, RL_PORT = 1, RR_PORT = 3;
    final int GYRO_PORT = 2;

    // driver station
    final int X_AXIS = 1, Y_AXIS = 2, TWIST = 3;
    final double JS_ROUND_TO = 0.01;

    // compressor
    final int COMPRESSOR_RELAY_CHANNEL = 7, PRESSURE_SWITCH_CHANNEL = 10;

    // intake solenoids
    final int   BLUE_INTAKE_FORWARD_PORT = 1,   BLUE_INTAKE_REVERSE_PORT = 2,
                ORANGE_INTAKE_FORWARD_PORT = 3, ORANGE_INTAKE_REVERSE_PORT = 4;

    // intake jaguars
    final int BLUE_INTAKE_JAG_PORT = 6, ORANGE_INTAKE_JAG_PORT = 5;

    // armor solenoids
    final int ARMOR_FORWARD_PORT = 5, ARMOR_REVERSE_PORT = 6;

    // ultrasonics
    final int ULTRASONIC_PORT = 3;

    // joystick port
    final int FIRST_JOYSTICK_PORT = 1, SECOND_JOYSTICK_PORT = 2;

    // joystick buttons
    final int   J1 = 1, J2 = 2, J3 = 3, J4 = 4,   J5 = 5,   J6 = 6,
                J7 = 7, J8 = 8, J9 = 9, J10 = 10, J11 = 11, J12 = 12;

    // TIMERS
    Timer gyroTimer, teleTimer, intakeTimer, armorTimer, blueTimer, orangeTimer;
    long startTime;

    // DRIVER STATION
    Joystick rightStick, leftStick;
    DriverStation ds;
    DriverStationLCD lcd;
    boolean hotGoalHot;
    Client client;

    // DRIVE BASE
    Drivetrain drivetrain;

    // SENSORS
    AnalogChannel ultrasonic;

    // INTAKE
    Jaguar blueIntake, orangeIntake;

    boolean activateIntake, outBlue, outOrange, blueExtended, orangeExtended,
            activatePneumatics, extendBlue, extendOrange;
    State state;

    // PNEUMATICS
    final Value reverse = DoubleSolenoid.Value.kForward,
                forward = DoubleSolenoid.Value.kReverse;

    Compressor compressor;

    DoubleSolenoid armor;
    DoubleSolenoid blueIntakeSolenoid, orangeIntakeSolenoid;

    boolean armorOut;

    public void robotInit() {
        // new driver station things
        ds          = DriverStation.getInstance();
        lcd         = DriverStationLCD.getInstance();
        rightStick  = new Joystick(FIRST_JOYSTICK_PORT);
        leftStick   = new Joystick(SECOND_JOYSTICK_PORT);
        client      = new Client(SERVER_PORT, SERVER_IP);

        // new timers
        gyroTimer   = new Timer();
        teleTimer   = new Timer();
        intakeTimer = new Timer();
        armorTimer  = new Timer();
        blueTimer   = new Timer();
        orangeTimer = new Timer();

        gyroTimer.start();
        teleTimer.start();
        intakeTimer.start();
        armorTimer.start();
        blueTimer.start();
        orangeTimer.start();

        // new variables
        armorOut = false;
        activateIntake = false;
        outBlue = false;
        outOrange = false;
        blueExtended = false;
        orangeExtended = false;
        activatePneumatics = false;
        extendBlue = false;
        extendOrange = false;
        hotGoalHot = false;

        // new mechanisms
        drivetrain = new Drivetrain(
            RL_PORT, RR_PORT, FL_PORT, FR_PORT,
            GYRO_PORT, SECS_TO_FULL_SPEED
        );

        blueIntake      = new Jaguar(BLUE_INTAKE_JAG_PORT);
        orangeIntake    = new Jaguar(ORANGE_INTAKE_JAG_PORT);

        armor = new DoubleSolenoid(ARMOR_FORWARD_PORT, ARMOR_REVERSE_PORT);

        blueIntakeSolenoid      = new DoubleSolenoid(
            BLUE_INTAKE_FORWARD_PORT, BLUE_INTAKE_REVERSE_PORT
        );

        orangeIntakeSolenoid    = new DoubleSolenoid(
            ORANGE_INTAKE_FORWARD_PORT, ORANGE_INTAKE_REVERSE_PORT
        );

        compressor = new Compressor(
            PRESSURE_SWITCH_CHANNEL, COMPRESSOR_RELAY_CHANNEL
        );

        state = new State();

        // new sensors
        ultrasonic = new AnalogChannel(ULTRASONIC_PORT);
    }

    public void autonomousInit() {
        drivetrain.gyro.reset();

        drivetrain.setSecsToFullSpeed(0.8);
        drivetrain.setInitSpeed(0);

        // begin mechanism operation
        compressor.start();

        startTime = System.currentTimeMillis();
        gyroTimer.reset();
        teleTimer.reset();
        intakeTimer.reset();
        armorTimer.stop();
        blueTimer.stop();
        orangeTimer.stop();

        client.isHot();
    }

    public void autonomousPeriodic() {
        activateIntake = true;
        activatePneumatics = true;
        double time = (System.currentTimeMillis() - startTime) / 1000.0;

        // get vision output
    	if (time < 0.1) {
            try {
                System.out.println("checking if hot");
                hotGoalHot = client.isHot();
                System.out.println(hotGoalHot);
            } catch (Exception ex) {
                System.out.println("Vision not responding.");
            }
        }

        if (time < 2.1) {
            drivetrain.drive(0, -0.8, 0.015);
        } else if (2.1 <= time && time <= 2.32) {
            drivetrain.drive(0, -0.5, 0.015);
        } else if (time < 3.5) {
            drivetrain.drive(0, -0.25, 0.015);
        } else {
            drivetrain.drive(0, 0, 0);
        }

        if (time > 2.5) {
            if (hotGoalHot){
                SmartDashboard.putString("Hot", "dump now");
                blueIntake.set(1);
                orangeIntake.set(-1);
            } else {
                SmartDashboard.putString("Not hot", "dump later");
                if (time > 6.42) { // wait until hot
                    blueIntake.set(1);
                    orangeIntake.set(-1);
                }
            }
        }

        putOnSmartDashboard();
    }

    public void teleopInit() {
        state.setState(state.BLUE);

        drivetrain.setSecsToFullSpeed(SECS_TO_FULL_SPEED);
        drivetrain.setInitSpeed(0.3);

        // begin mechanism operation
        compressor.start();

        gyroTimer.reset();
        teleTimer.reset();
        intakeTimer.reset();
        armorTimer.start();
        blueTimer.start();
        orangeTimer.start();
    }

    public void teleopPeriodic() {
        pressedButtons();

        drive();
        armor();
        intake();

        putOnSmartDashboard();
    }

    private void armor() {
        if (activatePneumatics) {
            if (armorOut) articulate(armor, reverse);
            else articulate(armor, forward);
        }
    }

    private void articulate(DoubleSolenoid solenoid, DoubleSolenoid.Value k) {
        solenoid.set(k);
    }

    private void intake() {
        if (activateIntake) {
            if (outBlue) {
                blueIntake.set(1);
                orangeIntake.set(-1);
            } else if (outOrange) {
                orangeIntake.set(1);
                blueIntake.set(-1);
            } else {
                setIntakes(-1);
            }
        }
        else setIntakes(0);

        if (activatePneumatics) {
            moveBlueIntake(extendBlue);
            moveOrangeIntake(extendOrange);
        }
    }

    private void moveBlueIntake(boolean out) {
        if (out) articulate(blueIntakeSolenoid, reverse);
        else articulate(blueIntakeSolenoid, forward);
    }

    private void moveOrangeIntake(boolean out) {
        if (out) articulate(orangeIntakeSolenoid, reverse);
        else articulate(orangeIntakeSolenoid, forward);
    }

    private void setIntakes(double speed) {
        blueIntake.set(speed);
        orangeIntake.set(speed);
    }

    private void pressedButtons() {
        activateIntake      = pressedRight(J1);
        activatePneumatics  = pressedRight(J2);
        int stateVal = state.getState();

        if (pressedRight(J2)) {
            if (stateVal == state.BLUE             && blueTimer.get() > 0.5) {
                blueTimer.reset();
                extendBlue = !extendBlue;
            } else if (stateVal == state.ORANGE    && orangeTimer.get() > 0.5) {
                orangeTimer.reset();
                extendOrange = !extendOrange;
            } else if (stateVal == state.ARMOR     && armorTimer.get() > 0.5) {
                armorTimer.reset();
                armorOut = !armorOut;
            }
        }

        if (pressedLeft(J2) || pressedLeft(J3)) {
            state.setState(state.ARMOR);
            outBlue = false;
            outOrange = false;
        }

        if (pressedLeft(J5)) {
            state.setState(state.BLUE);
            outBlue = true;
            outOrange = false;
        }
        if (pressedLeft(J4)) {
            state.setState(state.ORANGE);
            outOrange = true;
            outBlue = false;
        }

        if (rightStick.getRawAxis(4) > 0)
            compressor.stop();
        else
            compressor.start();

        if (pressedLeft(J1))
            drivetrain.REDUCE = 0.6;
        if (pressedRight(J11) || pressedRight(J12))
            drivetrain.REDUCE = 1;

        if (pressedLeft(10)) drivetrain.useGyro = true;
        if (pressedLeft(11)) drivetrain.useGyro = false;
    }

    private boolean pressedRight(int buttonNum) {
        return rightStick.getRawButton(buttonNum);
    }

    private boolean pressedLeft(int buttonNum) {
        return leftStick.getRawButton(buttonNum);
    }

    private double jsRound(double val) {
        // if JS_... is 0.01, mults val by 100, then floors, then divides by 100
        int sign = drivetrain.sign(val);
        val = Math.abs(val);

        return sign * Math.floor(val * (1 / JS_ROUND_TO)) / (1 / JS_ROUND_TO);
    }

    private void drive() {
        double x        = jsRound(rightStick.getRawAxis(X_AXIS));
        double y        = jsRound(rightStick.getRawAxis(Y_AXIS));
        double twist    = jsRound(rightStick.getRawAxis(TWIST));

        drivetrain.drive(x, y, twist);
    }

    private double getDistanceInches(AnalogChannel ultrasonic) {
        double voltage = ultrasonic.getAverageVoltage();
        voltage *= 1000; // convert to mV

        return voltage / MV_PER_INCH;
    }

    private void putOnSmartDashboard() {
        SmartDashboard.putNumber("joystick x", rightStick.getRawAxis(X_AXIS));
        SmartDashboard.putNumber("joystick y", rightStick.getRawAxis(Y_AXIS));
        SmartDashboard.putNumber("joystick z", rightStick.getRawAxis(TWIST));
        SmartDashboard.putNumber("avg voltage", ultrasonic.getAverageVoltage());
        SmartDashboard.putNumber("dist in inches",
                                getDistanceInches(ultrasonic));
        SmartDashboard.putNumber("gyro degrees", drivetrain.gyro.getAngle());
        SmartDashboard.putNumber("axis 4", rightStick.getRawAxis(4));
        SmartDashboard.putNumber("state", state.getState());
    }

    public static class State {
        public final int BLUE = 0, ORANGE = 1, ARMOR = 2;
        private int state;

        public State() {
            state = BLUE;
        }

        public int getState() {
            return state;
        }

        public void setState(int s) {
            state = s;
        }
    }

}

/**
 * This code was written by FIRST FRC Team 3501 (the Firebots) of Fremont High
 * School in Sunnyvale, California. This is for the 2014 FRC season's game,
 * Ultimate Ascent.
 *
 * ~ Jaidev P. Bapat, Software Team Lead
 */
