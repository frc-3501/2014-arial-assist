package edu.wpi.first.wpilibj.templates;
import java.io.*;
import javax.microedition.io.*;

public class Client {
    private SocketConnection connection;
    private InputStream input;
    private final String uri;

    public Client(int port, String ip) {
        uri = "socket://" + ip + ":" + Integer.toString(port);
    }

    public boolean isHot() {
        try {
            System.out.println("started isHot in client (inside method)");

            connection = (SocketConnection)
                    Connector.open(uri, Connector.READ, true);

            System.out.println("opened connector");
            input = connection.openInputStream();

            System.out.println("still working for some reason");

            int hot = input.read();
            System.out.println("stopped reading input");

            connection.close();
            input.close();

            return 1 == hot;
        } catch (Exception e) {
            System.out.println("Socket failed to open.");
            e.printStackTrace();
        }

        return true;
    }
}
